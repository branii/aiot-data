package cc.iotkit.data.service;

import cc.iotkit.data.model.TbChannel;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ChannelService extends IService<TbChannel>  {
}
