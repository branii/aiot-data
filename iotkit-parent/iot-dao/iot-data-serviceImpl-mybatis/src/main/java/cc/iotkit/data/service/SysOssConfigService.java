package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysOssConfig;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysOssConfigService extends IService<TbSysOssConfig> {
}
