package cc.iotkit.data.mapper;

import cc.iotkit.data.model.TbChannelTemplate;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChannelTemplateMapper extends BaseMapper<TbChannelTemplate> {
}
